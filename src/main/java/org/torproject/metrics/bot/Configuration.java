package org.torproject.metrics.bot;

import org.torproject.metrics.bot.irc.IrcChannelConfiguration;
import org.torproject.metrics.bot.mastodon.MastodonAccountConfiguration;
import org.torproject.metrics.bot.microblog.StatusType;
import org.torproject.metrics.bot.twitter.TwitterAccountConfiguration;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.apache.commons.configuration.XMLConfiguration;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * metrics-bot's configuration is loaded from an XML file in the home directory
 * of the user running metrics-bot. It is expected that metrics-bot will run
 * using a dedicated user and so keeping the home directory tidy with custom
 * paths is not a concern. The path to the configuration file is hardcoded as:
 * <code>~/.metrics-bot.xml</code>
 *
 * <p>The XML file contains credentials for the various accounts that are
 * connected to metrics-bot, so you should probably chmod it and make sure that
 * it's kept away from evil.
 *
 * <p>An example XML file looks like:
 *
 * <pre>
 * &lt;metrics-bot&gt;
 * &lt;twitter&gt;
 *  &lt;account name=&quot;@TorAtlas&quot; network=&quot;true&quot;
 *    relay=&quot;true&quot;&gt;
 *   &lt;consumerkey&gt;xxxxxxxxx&lt;/consumerkey&gt;
 *   &lt;consumersecret&gt;xxxxxxxxxx&lt;/consumersecret&gt;
 *   &lt;token&gt;xxxxxxxxxxx&lt;/token&gt;
 *   &lt;tokensecret&gt;xxxxxxxxxxxx&lt;/tokensecret&gt;
 *  &lt;/account&gt;
 * &lt;/twitter&gt;
 * &lt;mastodon&gt;
 *  &lt;account name=&quot;@metrics-bot@botsin.space&quot;
 *    network=&quot;true&quot; relay=&quot;true&quot;&gt;
 *   &lt;instancehost&gt;botsin.space&lt;/instancehost&gt;
 *   &lt;token&gt;xxxxxxxxxxxx&lt;/token&gt;
 *  &lt;/account&gt;
 * &lt;/mastodon&gt;
 * &lt;irc&gt;
 *  &lt;nick&gt;metrics-bot&lt;/nick&gt;
 *  &lt;password&gt;xxxxxxxxx&lt;/password&gt;
 *  &lt;channel name=&quot;#tor-test&quot; alert=&quot;true&quot; /&gt;
 * &lt;/irc&gt;
 * &lt;/metrics-bot&gt;
 * </pre>
 *
 * @author Iain R. Learmonth &lt;irl@torproject.org&gt;
 *
 */
public class Configuration {

  private static Configuration configuration;
  private XMLConfiguration xmlConfiguration;

  /**
   * Create a new configuration object. The path for the configuration file
   * is hardcoded to <code>~/.metrics-bot.xml</code>. It is expected that
   * this constructor will only ever be called at runtime by the
   * {@link #getSingleton()} static method.
   *
   * @throws IOException if there are errors reading the XML configuration
   *         from disk
   * @throws ConfigurationException if there are errors parsing the XML
   *         configuration
   */
  public Configuration() throws IOException, ConfigurationException {
    String homeDir = System.getProperty("user.home");
    Path configFilePath = Paths.get(homeDir).resolve(".metrics-bot.xml");
    xmlConfiguration = new XMLConfiguration(configFilePath.toString());
  }

  @SuppressWarnings("checkstyle:javadocmethod")
  public static Configuration getSingleton() {
    if (configuration == null) {
      try {
        configuration = new Configuration();
      } catch (IOException | ConfigurationException e) {
        // there's no point living if I don't have my configuration
        e.printStackTrace();
        System.exit(1);
      }
    }

    return configuration;
  }

  /**
   * Returns a list of Twitter account configurations that could
   * be used to instantiate Twitter account objects. Both the
   * authentication and status types are included from the XML
   * configuration.
   */
  public List<TwitterAccountConfiguration> getTwitterAccounts() {
    List<TwitterAccountConfiguration> accounts =
            new ArrayList<TwitterAccountConfiguration>();
    List<HierarchicalConfiguration> fields =
            xmlConfiguration.configurationsAt("twitter.account");
    for (HierarchicalConfiguration sub : fields) {
      List<StatusType> statusTypes = new ArrayList<StatusType>();
      if (sub.getString("[@alert]", "false").equals("true")) {
        statusTypes.add(StatusType.ALERT);
      }
      if (sub.getString("[@event]", "false").equals("true")) {
        statusTypes.add(StatusType.EVENT);
      }
      if (sub.getString("[@network]", "false").equals("true")) {
        statusTypes.add(StatusType.NETWORK);
      }
      if (sub.getString("[@relay]", "false").equals("true")) {
        statusTypes.add(StatusType.RELAY);
      }
      TwitterAccountConfiguration account =
              new TwitterAccountConfiguration(sub.getString("[@name]"),
                      statusTypes, sub.getString("consumerkey"),
                      sub.getString("consumersecret"), sub.getString("token"),
                      sub.getString("tokensecret"));
      accounts.add(account);
    }
    return accounts;
  }

  /**
   * Returns a list of Mastodon account configurations that could
   * be used to instantiate Mastodon account objects. Both the
   * authentication and status types are included from the XML
   * configuration.
   */
  public List<MastodonAccountConfiguration> getMastodonAccounts() {
    List<MastodonAccountConfiguration> accounts =
            new ArrayList<MastodonAccountConfiguration>();
    List<HierarchicalConfiguration> fields =
            xmlConfiguration.configurationsAt("mastodon.account");
    for (HierarchicalConfiguration sub : fields) {
      List<StatusType> statusTypes = new ArrayList<StatusType>();
      if (sub.getString("[@alert]", "false").equals("true")) {
        statusTypes.add(StatusType.ALERT);
      }
      if (sub.getString("[@event]", "false").equals("true")) {
        statusTypes.add(StatusType.EVENT);
      }
      if (sub.getString("[@network]", "false").equals("true")) {
        statusTypes.add(StatusType.NETWORK);
      }
      if (sub.getString("[@relay]", "false").equals("true")) {
        statusTypes.add(StatusType.RELAY);
      }
      MastodonAccountConfiguration account =
              new MastodonAccountConfiguration(sub.getString("[@name]"),
                      statusTypes, sub.getString("instancehost"),
                      sub.getString("token"));
      accounts.add(account);
    }
    return accounts;
  }

  public String getIrcNick() {
    return xmlConfiguration.getString("irc.nick");
  }

  public String getIrcNickServPassword() {
    return xmlConfiguration.getString("irc.password");
  }

  /**
   * Returns a list of IRC channel configurations that could
   * be used to instantiate IRC channel objects. Both the
   * authentication and status types are included from the XML
   * configuration.
   */
  public List<IrcChannelConfiguration> getIrcChannels() {
    List<IrcChannelConfiguration> channels =
            new ArrayList<IrcChannelConfiguration>();
    List<HierarchicalConfiguration> fields =
            xmlConfiguration.configurationsAt("irc.channel");
    for (HierarchicalConfiguration sub : fields) {
      List<StatusType> statusTypes = new ArrayList<StatusType>();
      if (sub.getString("[@alert]", "false").equals("true")) {
        statusTypes.add(StatusType.ALERT);
      }
      if (sub.getString("[@event]", "false").equals("true")) {
        statusTypes.add(StatusType.EVENT);
      }
      if (sub.getString("[@network]", "false").equals("true")) {
        statusTypes.add(StatusType.NETWORK);
      }
      if (sub.getString("[@relay]", "false").equals("true")) {
        statusTypes.add(StatusType.RELAY);
      }
      IrcChannelConfiguration channel =
              new IrcChannelConfiguration(sub.getString("[@name]"), statusTypes,
                      sub.getString("[@name]"));
      channels.add(channel);
    }
    return channels;
  }
}
