package org.torproject.metrics.bot.twitter;

import org.torproject.metrics.bot.microblog.MicroblogAccountConfiguration;
import org.torproject.metrics.bot.microblog.StatusType;

import java.util.List;

public class TwitterAccountConfiguration
        implements MicroblogAccountConfiguration {

  private String friendlyName;
  private List<StatusType> statusTypes;
  private String consumerKey;
  private String consumerSecret;
  private String token;
  private String tokenSecret;

  @SuppressWarnings("checkstyle:javadocmethod")
  public TwitterAccountConfiguration(String friendlyName,
          List<StatusType> statusTypes, String consumerKey,
          String consumerSecret, String token, String tokenSecret) {
    this.friendlyName = friendlyName;
    this.consumerKey = consumerKey;
    this.consumerSecret = consumerSecret;
    this.token = token;
    this.tokenSecret = tokenSecret;
    this.statusTypes = statusTypes;
  }

  public String getFriendlyName() {
    return friendlyName;
  }

  public String getConsumerKey() {
    return consumerKey;
  }

  public String getConsumerSecret() {
    return consumerSecret;
  }

  public String getToken() {
    return token;
  }

  public String getTokenSecret() {
    return tokenSecret;
  }

  public List<StatusType> getStatusTypes() {
    return statusTypes;
  }

  @Override
  public String toString() {
    return "<TwitterAccountConfiguration name=" + friendlyName
            + ", consumerKey=" + consumerKey + ", consumerSecret="
            + consumerSecret + ", token=" + token + ", tokenSecret="
            + tokenSecret + ">";
  }

}
