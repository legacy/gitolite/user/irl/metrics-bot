package org.torproject.metrics.bot.tor;

import java.awt.image.RenderedImage;

public interface Country extends RelayAggregate {

  String getCountry();

  String getCountryName();

  RenderedImage generateBadge();
}