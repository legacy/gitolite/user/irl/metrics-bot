package org.torproject.metrics.bot.tor;

import java.util.List;

public class RelayAggregateImpl implements RelayAggregate {

  protected List<Relay> relays;

  public RelayAggregateImpl() {
    super();
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.torproject.metrics.bot.tor.RelayAggregate#getAdvertisedBandwidth()
   */
  @Override
  public Long getAdvertisedBandwidth() {
    return relays.stream().mapToLong(r -> r.getAdvertisedBandwidth()).sum();
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.torproject.metrics.bot.tor.RelayAggregate#getGuardProbability()
   */
  @Override
  public Double getGuardProbability() {
    return relays.stream().mapToDouble(r -> r.getGuardProbability()).sum();
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.torproject.metrics.bot.tor.RelayAggregate#getMiddleProbability()
   */
  @Override
  public Double getMiddleProbability() {
    return relays.stream().mapToDouble(r -> r.getMiddleProbability()).sum();
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.torproject.metrics.bot.tor.RelayAggregate#getExitProbability()
   */
  @Override
  public Double getExitProbability() {
    return relays.stream().mapToDouble(r -> r.getExitProbability()).sum();
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.torproject.metrics.bot.tor.RelayAggregate#getRelayCount()
   */
  @Override
  public Integer getRelayCount() {
    return relays.size();
  }

}