package org.torproject.metrics.bot.tor;

import java.util.List;

public interface Bridge extends BaseRelay {

  /**
   * Get a list of of (pluggable) transport names supported by this bridge.
   * 
   * @return A list of pluggable transport names.
   */
  List<String> getTransports();

  Boolean hasIPv4Listener();

  Boolean hasIPv6Listener();

}