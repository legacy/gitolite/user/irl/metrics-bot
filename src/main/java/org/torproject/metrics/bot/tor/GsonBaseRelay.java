package org.torproject.metrics.bot.tor;

import org.torproject.metrics.bot.MetricsBot;
import org.torproject.metrics.bot.microblog.MicroblogUtils;
import org.torproject.metrics.bot.tor.BaseRelay;
import org.torproject.metrics.bot.tor.RelayFlag;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.imageio.ImageIO;

abstract class GsonBaseRelay implements BaseRelay {

  String nickname;
  String fingerprint;
  String[] orAddresses;
  ZonedDateTime lastSeen;
  ZonedDateTime firstSeen;
  Boolean running;
  String[] flags;
  ZonedDateTime lastRestarted;
  Long advertisedBandwidth;
  String platform;
  protected Boolean bridge;
  
  public GsonBaseRelay() {
    bridge = false;
  }

  @Override
  public String getNickname() {
    return nickname;
  }

  @Override
  public String getFingerprint() {
    return fingerprint;
  }

  @Override
  public List<String> getOrAddresses() {
    return Arrays.asList(orAddresses);
  }

  @Override
  public ZonedDateTime getLastSeen() {
    return lastSeen;
  }

  @Override
  public ZonedDateTime getFirstSeen() {
    return firstSeen;
  }

  @Override
  public Boolean isRunning() {
    return running;
  }

  @Override
  public List<String> getFlagStrings() {
    return Arrays.asList(flags);
  }

  @Override
  public List<RelayFlag> getFlags() {
    // The Guard, Exit and HSDir flags do not have meaning for bridges, and
    // might confuse people if seen in relation to them. See: #24886.
    List<RelayFlag> flags = new ArrayList<RelayFlag>();
    for (String flag : this.flags) {
      switch (flag) {
        case "Exit":
          if (!bridge) {
            flags.add(RelayFlag.EXIT);
          }
          break;
        case "Guard":
          if (!bridge) {
            flags.add(RelayFlag.GUARD);
          }
          break;
        case "Fast":
          flags.add(RelayFlag.FAST);
          break;
        case "Stable":
          flags.add(RelayFlag.STABLE);
          break;
        case "V2Dir":
          flags.add(RelayFlag.V2DIR);
          break;
        case "Valid":
          flags.add(RelayFlag.VALID);
          break;
        case "HSDir":
          if (!bridge) {
            flags.add(RelayFlag.HSDIR);
          }
          break;
        case "Authority":
          flags.add(RelayFlag.AUTHORITY);
          break;
        case "Running":
          flags.add(RelayFlag.RUNNING);
          break;
        case "BadExit":
          flags.add(RelayFlag.BADEXIT);
          break;
        default:
          MetricsBot.error("Saw an unknown flag: " + flag);
          break;
      }
    }
    return flags;
  }

  @Override
  public ZonedDateTime getLastRestarted() {
    return lastRestarted;
  }

  @Override
  public Long getAdvertisedBandwidth() {
    return advertisedBandwidth;
  }

  @Override
  public String getPlatform() {
    return platform;
  }

  @Override
  public abstract String humanString(Boolean withRelaySearchUrl);
  
  @Override
  public RenderedImage generateBadge() {
    BufferedImage badge =
            new BufferedImage(512, 400, BufferedImage.TYPE_INT_RGB);
    Graphics2D g2 = badge.createGraphics();

    try {
      drawBadgeBase(g2);
    } catch (IOException e) {
      return null;
    }
    drawBadgeFlags(g2);
    drawBadgeSpecifics(g2);

    return (RenderedImage) badge;
  }

  protected void drawBadgeBase(Graphics2D g2) throws IOException {
    // Set up graphics
    g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
            RenderingHints.VALUE_ANTIALIAS_ON);

    g2.setColor(new Color(255, 255, 255));
    g2.fillRect(0, 0, 512, 400);

    g2.setColor(new Color(25, 25, 25));
    g2.fillRect(0, 0, 512, 320);

    g2.setColor(new Color(255, 255, 255));
    g2.setFont(new Font("Source Sans Pro", Font.BOLD, 50));
    int nicknameWidth = g2.getFontMetrics().stringWidth(getNickname());
    if (60 + 12 + nicknameWidth > 430) {
      g2.setFont(new Font("Source Sans Pro", Font.BOLD, 40));
      nicknameWidth = g2.getFontMetrics().stringWidth(getNickname());
    }
    g2.drawString(getNickname(), 45, 80);
    
    g2.setColor(new Color(102, 221, 42));
    g2.fillOval(60 + nicknameWidth, 60, 12, 12);

    // Add the onion
    if (60 + 12 + nicknameWidth < 430) {
      ClassLoader cl = this.getClass().getClassLoader();
      BufferedImage overlayImage;
      try {
        overlayImage = ImageIO.read(cl.getResource("flat-onion.png"));
        g2.drawImage(overlayImage, 435, 45, null);
      } catch (IOException e) {
        // Not too upset if we can't load the onion
        e.printStackTrace();
      }
    }

    g2.setColor(new Color(94, 94, 94));
    g2.setFont(new Font("Source Code Pro", Font.PLAIN, 14));
    g2.drawString(getFingerprint(), 45, 110);

    g2.setColor(new Color(125, 70, 152));
    g2.setFont(new Font("Source Sans Pro", Font.BOLD, 12));
    g2.drawString("Advertised Bandwidth".toUpperCase(), 45, 175);
    g2.drawString("Age".toUpperCase(), 300, 175);

    g2.setColor(new Color(255, 255, 255));
    g2.setFont(new Font("Source Sans Pro", Font.BOLD, 45));

    String formattedAdvertisedBandwidth =
            MicroblogUtils.formatBandwidth(getAdvertisedBandwidth());
    String formattedAge = MicroblogUtils.formatAge(getFirstSeen());

    g2.drawString(formattedAdvertisedBandwidth.split(" ")[0], 45, 220);
    g2.drawString(">" + formattedAge.split(" and ")[0].split(" ")[0], 300, 220);

    int bandwidthWidth =
            g2.getFontMetrics()
                    .stringWidth(formattedAdvertisedBandwidth.split(" ")[0]);
    int ageWidth =
            g2.getFontMetrics()
                    .stringWidth(formattedAge.split(" and ")[0].split(" ")[0]);

    g2.setColor(new Color(255, 255, 255));
    g2.setFont(new Font("Source Sans Pro ExtraLight", Font.PLAIN, 45));
    g2.drawString(formattedAdvertisedBandwidth.split(" ")[1],
            55 + bandwidthWidth, 220);
    g2.drawString(formattedAge.split(" and ")[0].split(" ")[1], 340 + ageWidth,
            220);
  }

  protected void drawBadgeSpecifics(Graphics2D g2) {
    throw new UnsupportedOperationException();
  }

  protected void drawBadgeFlags(Graphics2D g2) {
    int flagsX = 0;
    g2.setColor(new Color(125, 70, 152));
    g2.setFont(new Font("Source Sans Pro", Font.BOLD, 12));
    for (RelayFlag flag : getFlags()) {
      try {
        g2.drawImage(flag.getLargeImage(), 25 + (70 * flagsX), 335, null);
      } catch (IOException e) {
        MetricsBot
                .error("Failed to create a badge, relay flag image is missing: "
                        + flag.getHumanName());
        e.printStackTrace();
        // won't do the icon, not a disaster
      }
      int flagWidth =
              g2.getFontMetrics()
                      .stringWidth(flag.getHumanName().toUpperCase());
      try {
        g2.drawString(flag.getHumanName().toUpperCase(),
                (int) (25
                        + (flag.getLargeImage().getWidth(null) / 2)
                        + (flagsX * 70) - (flagWidth * 0.5)),
                385);
      } catch (IOException e) {
        // won't do the name, also not a disaster
        e.printStackTrace();
      }
      flagsX += 1;
    }
  }
  
  @Override
  public String toString() {
    return "<GsonBaseRelay nickname="
            + nickname + ", fingerprint=" + fingerprint + ", orAddresses="
            + Arrays.toString(orAddresses) + ", lastSeen=" + lastSeen
            + ", firstSeen=" + firstSeen + ", running=" + running + ", flags="
            + Arrays.toString(flags) + ", lastRestarted=" + lastRestarted
            + ", advertisedBandwidth=" + advertisedBandwidth + ", platform="
            + platform + ">";
  }
}