package org.torproject.metrics.bot.tor;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

class GsonExitPolicySummary implements ExitPolicySummary {
  String[] accept;
  String[] reject;

  /* (non-Javadoc)
   * @see org.torproject.metrics.bot.tor.ExitPolicySummary#getAcceptPorts()
   */
  @Override
  public int[] getAcceptPorts() {
    return portListToIntArray(accept);
  }
  
  /* (non-Javadoc)
   * @see org.torproject.metrics.bot.tor.ExitPolicySummary#getRejectPorts()
   */
  @Override
  public int[] getRejectPorts() {
    return portListToIntArray(reject);
  }

  private int[] portListToIntArray(String[] portList) {
    return Arrays.asList(portList).stream().mapToInt(i -> Integer.parseInt(i))
            .toArray();
  }
  
  /* (non-Javadoc)
   * @see org.torproject.metrics.bot.tor.ExitPolicySummary#toMap()
   */
  @Override
  public Map<String, String[]> toMap() {
    Map<String, String[]> map = new HashMap<String, String[]>();
    map.put("accept", accept);
    map.put("reject", reject);
    return map;  
  }
  
  /* (non-Javadoc)
   * @see org.torproject.metrics.bot.tor.ExitPolicySummary#toSimpleMap()
   */
  @Override
  public Map<String, String> toSimpleMap() {
    Map<String, String> map = new HashMap<String, String>();
    map.put("accept", String.join("\n", accept));
    map.put("reject", String.join("\n", reject));
    return map;
  }

  @Override
  public String toString() {
    return "<GsonExitPolicySummary accept="
            + Arrays.toString(accept) + ", reject=" + Arrays.toString(reject)
            + ">";
  }

}