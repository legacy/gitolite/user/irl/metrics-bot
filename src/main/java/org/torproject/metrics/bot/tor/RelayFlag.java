
package org.torproject.metrics.bot.tor;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

/**
 *
 * @author Iain R. Learmonth &lt;Iain R. Learmonth&gt;
 */
public enum RelayFlag {

  EXIT("Exit", "exit"), FAST("Fast", "fast"), GUARD("Guard", "guard"), STABLE(
          "Stable", "stable"), V2DIR("V2Dir", "v2dir"), RUNNING("Running",
                  "running"), VALID("Valid", "valid"), HSDIR("HSDir",
                          "hsdir"), AUTHORITY("Authority",
                                  "authority"), BADEXIT("BadExit", "badexit");

  private final String humanName;
  private final String imageName;

  RelayFlag(String flagName, String imageName) {
    this.humanName = flagName;
    this.imageName = imageName;
  }

  public String getHumanName() {
    return humanName;
  }

  /**
   * Return a pictogram representing the flag. (16 pixels high, variable width
   * less than or equal to 16 pixels)
   */
  public Image getImage() throws IOException {
    ClassLoader cl = this.getClass().getClassLoader();
    BufferedImage flagImage =
            ImageIO.read(cl.getResource("flags/" + imageName + ".png"));
    return flagImage;
  }

  /**
   * Return a larger pictogram representing the flag. (32 pixels high, variable
   * width less than or equal to 32 pixels)
   */
  public Image getLargeImage() throws IOException {
    ClassLoader cl = this.getClass().getClassLoader();
    BufferedImage flagImage =
            ImageIO.read(cl.getResource("large_flags/" + imageName + ".png"));
    return flagImage;
  }
}
