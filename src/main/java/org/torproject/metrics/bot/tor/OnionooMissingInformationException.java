package org.torproject.metrics.bot.tor;

/**
 * Thrown if information was missing from a details or summary object that was
 * required, most likely because Onionoo did not have the information available.
 * 
 * @author Iain R. Learmonth &lt;irl@torproject.org&gt;
 */
public class OnionooMissingInformationException extends Exception {
  private static final long serialVersionUID = 189662730138275674L;
}
