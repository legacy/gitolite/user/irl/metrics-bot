package org.torproject.metrics.bot.tor;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Map;

public interface Relay extends BaseRelay {

  /**
   * Get a list of IPv4 or IPv6 addresses that the relay used to exit to the
   * Internet in the past 24 hours. IPv6 hex characters are all lower-case. Only
   * those addresses are listed that are different from onion-routing addresses.
   * 
   * @return A list of IPv4 or IPv6 addresses.
   */
  List<String> getExitAddresses();

  /**
   * Get the IPv4 address and TCP port where the relay accepts directory
   * connections.
   * 
   * @return An IPv4 address and TCP port.
   */
  String getDirAddress();

  /**
   * Get the time when the relay last stopped announcing an IPv4 or IPv6 address
   * or TCP port where it previously accepted onion-routing or directory
   * connections. This timestamp can serve as indicator whether this relay would
   * be a suitable fallback directory.
   * 
   * @return A UTC date and time.
   */
  ZonedDateTime getLastChangedAddressOrPort();

  /**
   * Get whether or not this relay indicated that it is hibernating in its last
   * known server descriptor. This information may be helpful to decide whether
   * a relay that is not running anymore has reached its accounting limit and
   * has not dropped out of the network for another, unknown reason.
   * 
   * @return True if the relay indicated it is hibernating, otherwise false.
   */
  Boolean isHibernating();

  /**
   * Get the country code as found by Onionoo in a GeoIP database by
   * resolving the relay's first onion-routing IP address.
   * 
   * @return A two-letter country code.
   * @see #getCountryName()
   */
  String getCountry();

  /**
   * Get the country name as found by Onionoo in a GeoIP database by
   * resolving the relay's first onion-routing IP address.
   * 
   * @return A country name.
   * @see #getRegionName()
   * @see #getCityName()
   */
  String getCountryName();

  /**
   * Get the region name as found by Onionoo in a GeoIP database by
   * resolving the relay's first onion-routing IP address.
   * 
   * @return A region name.
   * @see #getCountryName()
   * @see #getCityName()
   */
  String getRegionName();

  /**
   * Get the city name as found by Onionoo in a GeoIP database by resolving
   * the relay's first onion-routing IP address.
   * 
   * @return A city name.
   * @see #getCountryName()
   * @see #getRegionName()
   */
  String getCityName();

  /**
   * Get the latitude as found by Onionoo in a GeoIP database by resolving
   * the relay's first onion-routing IP address.
   * 
   * @return A latitude.
   * @see #getLongitude()
   */
  Double getLatitude();

  /**
   * Get the longitude as found by Onionoo in a GeoIP database by resolving
   * the relay's first onion-routing IP address.
   * 
   * @return A longitude.
   * @see #getLatitude()
   */
  Double getLongitude();

  /**
   * Get the AS number as found by Onionoo in an AS database by resolving
   * the relay's first onion-routing IP address.
   * 
   * @return An AS number prefixed by "AS".
   * @see #getAsName()
   */
  String getAsNumber();

  /**
   * Get the AS name as found by Onionoo in an AS database by resolving the
   * relay's first onion-routing IP address.
   * 
   * @return An AS name.
   * @see #getAsNumber()
   */
  String getAsName();

  /**
   * Get the weight assigned to this relay by the directory authorities that
   * clients use in their path selection algorithm.
   * 
   * @return A consensus weight. The unit is arbitrary; currently it's kilobytes
   *         per second, but that might change in the future.
   * @see #getConsensusWeightFraction()
   */
  Long getConsensusWeight();

  /**
   * Get the host name as found by Onionoo in a reverse DNS lookup of the
   * relay IP address. This field is updated at most once in 12 hours, unless
   * the relay IP address changes.
   * 
   * @return A DNS name or IP address.
   */
  String getHostName();

  /**
   * Get the average bandwidth that this relay is willing to sustain over long
   * periods.
   * 
   * @return A bandwidth in bytes per second.
   */
  Long getBandwidthRate();

  /**
   * Get the bandwidth that this relay is willing to sustain in very short
   * intervals.
   * 
   * @return A bandwidth in bytes per second.
   */
  Long getBandwidthBurst();

  /**
   * Get the bandwidth estimate of the capacity this relay can handle. The relay
   * remembers the maximum bandwidth sustained output over any ten second period
   * in the past day, and another sustained input. The "observed_bandwidth"
   * value is the lesser of these two numbers.
   * 
   * @return A bandwidth in bytes per second.
   */
  Long getObservedBandwidth();

  /**
   * Get a list of exit-policy lines. May contradict the "exit_policy_summary"
   * field in a rare edge case: this happens when the relay changes its exit
   * policy after the directory authorities summarized the previous exit policy.
   * 
   * @return A list of exit-policy lines.
   */
  List<String> getExitPolicy();

  /**
   * Get a summary version of the relay's exit policy containing a dictionary
   * with either an "accept" or a "reject" element. If there is an "accept"
   * ("reject") element, the relay accepts (rejects) all TCP ports or port
   * ranges in the given list for most IP addresses and rejects (accepts) all
   * other ports. May contradict the "exit_policy" field in a rare edge case:
   * this happens when the relay changes its exit policy after the directory
   * authorities summarized the previous exit policy.
   * 
   * @return A list of summary exit-policy lines.
   */
  Map<String, String> getExitPolicySummary();

  /**
   * Get a summary version of the relay's IPv6 exit policy containing a
   * dictionary with either an "accept" or a "reject" element. If there is an
   * "accept" ("reject") element, the relay accepts (rejects) all TCP ports or
   * port ranges in the given list for most IP addresses and rejects (accepts)
   * all other ports. May contradict the "exit_p)olicy_summary" field in a rare
   * edge case: this happens when the relay changes its exit policy after the
   * directory authorities summarized the previous exit policy.
   * 
   * @return A list of summary exit-policy lines.
   */
  Map<String, String> getExitPolicyV6Summary();

  /**
   * Get the contact address of the relay operator as configured by the relay
   * operator.
   * 
   * @return An arbitrarily formated string.
   */
  String getContact();

  /**
   * Get whether or not the Tor software version of this relay is recommended by
   * the directory authorities.
   * 
   * @return True if the version is a recommended version, otherwise false.
   * @see BaseRelay#getPlatform()
   */
  Boolean isRecommendedVersion();

  /**
   * Get a list of relays that are in an effective, mutual family relationship
   * with this relay. These relays are part of this relay's family and they
   * consider this relay to be part of their family.
   * 
   * @return A list of relay fingerprints.
   * @see #getAllegedFamily()
   * @see #getIndirectFamily()
   */
  List<String> getEffectiveFamily();

  /**
   * Get a list of relays that are not in an effective, mutual family
   * relationship with this relay. These relays are part of this relay's family
   * but they don't consider this relay to be part of their family.
   * 
   * @return A list of relay fingerprints.
   * @see #getEffectiveFamily()
   * @see #getIndirectFamily()
   */
  List<String> getAllegedFamily();

  /**
   * Get a list of relays that are not in an effective, mutual family
   * relationship with this relay but that can be reached by following
   * effective, mutual family relationships starting at this relay.
   * 
   * @return A list of relay fingerprints.
   * @see #getEffectiveFamily()
   * @see #getAllegedFamily()
   */
  List<String> getIndirectFamily();

  /**
   * Get the fraction of this relay's consensus weight compared to the sum of
   * all consensus weights in the network. This fraction is a very rough
   * approximation of the probability of this relay to be selected by clients.
   * 
   * @return A decimal fraction.
   * @see #getConsensusWeight()
   */
  Double getConsensusWeightFraction();

  /**
   * Get the Probability of the relay to be selected for the guard position.
   * This probability is calculated based on consensus weights, relay flags, and
   * bandwidth weights in the consensus. Path selection depends on more factors,
   * so that this probability can only be an approximation.
   * 
   * @return A decimal fraction.
   * @see #getMiddleProbability()
   * @see #getExitProbability()
   */
  Double getGuardProbability();

  /**
   * Get the probability of this relay to be selected for the middle position.
   * This probability is calculated based on consensus weights, relay flags, and
   * bandwidth weights in the consensus. Path selection depends on more factors,
   * so that this probability can only be an approximation.
   * 
   * @return A decimal fraction.
   * @see #getGuardProbability()
   * @see #getExitProbability()
   */
  Double getMiddleProbability();

  /**
   * Probability of this relay to be selected for the exit position. This
   * probability is calculated based on consensus weights, relay flags, and
   * bandwidth weights in the consensus. Path selection depends on more factors,
   * so that this probability can only be an approximation.
   * 
   * @return A decimal fraction.
   * @see #getGuardProbability()
   * @see #getMiddleProbability()
   */
  Double getExitProbability();

  /**
   * Get whether or not the consensus weight of this relay is based on a
   * threshold of 3 or more measurements by Tor bandwidth authorities.
   * 
   * @return True if the relay has been measured by 3 or more measurements by
   *         Tor bandwidth authorities, otherwise false.
   * @see #getConsensusWeight()
   */
  Boolean getMeasured();

}