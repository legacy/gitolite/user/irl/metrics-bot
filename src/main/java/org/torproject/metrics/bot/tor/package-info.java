/**
 * This package contains classes representing Tor network concepts (relays,
 * bridges, flags, etc.). The primary implementation of these classes is
 * backed by Onionoo as a data source and uses Gson as a parser internally.
 */
package org.torproject.metrics.bot.tor;
