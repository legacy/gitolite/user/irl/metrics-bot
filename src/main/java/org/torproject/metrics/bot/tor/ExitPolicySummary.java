package org.torproject.metrics.bot.tor;

import java.util.Map;

public interface ExitPolicySummary {

  int[] getAcceptPorts();

  int[] getRejectPorts();

  Map<String, String[]> toMap();

  Map<String, String> toSimpleMap();

}