package org.torproject.metrics.bot.tor;

public interface RelayAggregate {

  Long getAdvertisedBandwidth();

  Double getGuardProbability();

  Double getMiddleProbability();

  Double getExitProbability();

  Integer getRelayCount();

}