package org.torproject.metrics.bot.tor;

import java.awt.image.RenderedImage;
import java.time.ZonedDateTime;
import java.util.List;

public interface BaseRelay {

  /**
   * Get the relay's nickname.
   * 
   * @return A relay nickname consisting of 1-19 alphanumerical characters.
   */
  String getNickname();

  /**
   * Get the relay's fingerprint. In the case of bridge relays, a SHA-1 hash of
   * the fingerprint will be returned.
   * 
   * @return A relay fingerprint or hashed fingerprint consisting of 40
   *         upper-case hexadecimal characters.
   */
  String getFingerprint();

  /**
   * Get a list of IPv4 or IPv6 addresses and TCP ports where the relay accepts
   * onion-routing connections. The first address is the primary onion-routing
   * address that the relay used to register in the network, subsequent
   * addresses are in arbitrary order. IPv6 hex characters are all lower-case.
   * In the case of bridge relays, IP addresses will be sanitized. Sanitized IP
   * addresses are always in 10/8 or [fd9f:2e19:3bcf/48] IP networks and are
   * only useful to learn which IP version the bridge uses and to detect whether
   * the bridge changed its address. Sanitized IP addresses always change on the
   * 1st of every month at 00:00:00 UTC, regardless of the bridge actually
   * changing its IP address. TCP ports are not sanitized.
   * 
   * @return A list of IPv4 or IPv6 addresses and TCP ports.
   */
  List<String> getOrAddresses();

  /**
   * Get the time the relay was last seen in a network status consensus.
   * 
   * @return A UTC date and time.
   */
  ZonedDateTime getLastSeen();

  /**
   * Get the time the relay was first seen in a network status consensus.
   * 
   * @return A UTC date and time.
   */
  ZonedDateTime getFirstSeen();

  /**
   * Get whether or not the relay is currently running in the latest network
   * status consensus.
   * 
   * @return True if the relay is running, otherwise false.
   */
  Boolean isRunning();

  /**
   * Get the list of flags that the directory authorities assigned to this
   * relay.
   * 
   * @return A list of flags.
   */
  List<String> getFlagStrings();

  /**
   * Get the list of flags that the directory authorities assigned to this
   * relay.
   * 
   * @return A list of flags.
   * @see #getFlagStrings
   */
  List<RelayFlag> getFlags();

  /**
   * Get the time the relay was last (re-)started.
   * 
   * @return A UTC date and time.
   */
  ZonedDateTime getLastRestarted();

  /**
   * Bandwidth that this relay is willing and capable to provide. This bandwidth
   * value is the minimum of bandwidth_rate, bandwidth_burst, and
   * observed_bandwidth.
   * 
   * @return A bandwidth in bytes per second.
   */
  Long getAdvertisedBandwidth();

  /**
   * Get platform and operating system information for the relay.
   * 
   * @return Platform string containing operating system and Tor version
   *         details.
   * @see RelayImpl#isRecommendedVersion()
   */
  String getPlatform();

  RenderedImage generateBadge();

  /**
   * Returns a human readable string with interesting information about the
   * relay. This is intended to be provided in response to a user request, for
   * example in an IRC channel.
   * 
   * @param withRelaySearchUrl
   *          true if an Relay Search URL should be included, otherwise
   *          false
   * @return a human readable string
   */
  String humanString(Boolean withRelaySearchUrl);

}