package org.torproject.metrics.bot.tor;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.util.Arrays;
import java.util.List;

class GsonBridge extends GsonBaseRelay implements Bridge {

  String hashedFingerprint;
  String[] transports;

  public GsonBridge() {
    super();
    this.bridge = true;
  }

  public String getFingerprint() {
    return hashedFingerprint;
  }
  
  @Override
  public List<String> getTransports() {
    return Arrays.asList(transports);
  }

  @Override
  public Boolean hasIPv4Listener() {
    for (String address : getOrAddresses()) {
      if (address.startsWith("10.")) {
        return true;
      }
    }
    return false;
  }

  @Override
  public Boolean hasIPv6Listener() {
    for (String address : getOrAddresses()) {
      if (address.startsWith("fd9f:2e19:3bcf:")) {
        return true;
      }
    }
    return false;
  }

  @Override
  public String humanString(Boolean withRelaySearchUrl) {
    String flags = "";
    for (RelayFlag flag : getFlags()) {
      flags += flag.getHumanName() + " ";
    }
    String humanString =
            getNickname() + " - Bridge - CW: " + " [ " + flags + "]";
    if (withRelaySearchUrl) {
      humanString +=
              " https://metrics.torproject.org/rs.html#details/"
                      + getFingerprint();
    }
    return humanString;
  }

  protected void drawBadgeSpecifics(Graphics2D g2) {
    drawBadgeTransport(g2);
  }

  private void drawBadgeTransport(Graphics2D g2) {
    // Draw headings
    g2.setColor(new Color(125, 70, 152));
    g2.setFont(new Font("Source Sans Pro", Font.BOLD, 12));
    g2.drawString("Transport".toUpperCase(), 45, 260);
    g2.drawString("IPv4", 215, 260);
    g2.drawString("IPv6", 385, 260);

    // Draw values
    g2.setColor(new Color(255, 255, 255));
    g2.setFont(new Font("Source Sans Pro Light", Font.PLAIN, 26));
    g2.drawString(getTransports().get(0), 45, 290);
    g2.drawString((hasIPv4Listener()) ? "Yes" : "No", 215, 290);
    g2.drawString((hasIPv6Listener()) ? "Yes" : "No", 385, 290);
  }
  
  @Override
  public String toString() {
    return "<GsonBridge nickname="
            + getNickname() + ", fingerprint=" + getFingerprint()
            + ", orAddresses=" + getOrAddresses() + ", lastSeen="
            + getLastSeen() + ", firstSeen=" + getFirstSeen() + ", running="
            + isRunning() + ", flags=" + getFlags() + ", lastRestarted="
            + getLastRestarted() + ", advertisedBandwidth="
            + getAdvertisedBandwidth() + ", platform=" + getPlatform()
            + ", transports=" + Arrays.toString(transports)
            + ", hasIPv4Listener()=" + hasIPv4Listener()
            + ", hasIPv6Listener()=" + hasIPv6Listener() + ">";
  }

}
