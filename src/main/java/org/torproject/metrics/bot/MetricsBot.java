package org.torproject.metrics.bot;

import org.torproject.metrics.bot.factoids.FactoidConfiguration;
import org.torproject.metrics.bot.factoids.FactoidManager;
import org.torproject.metrics.bot.irc.IrcChannel;
import org.torproject.metrics.bot.irc.IrcChannelConfiguration;
import org.torproject.metrics.bot.irc.IrcListener;
import org.torproject.metrics.bot.mastodon.MastodonAccount;
import org.torproject.metrics.bot.mastodon.MastodonAccountConfiguration;
import org.torproject.metrics.bot.microblog.MicroblogAccount;
import org.torproject.metrics.bot.microblog.MicroblogStatus;
import org.torproject.metrics.bot.microblog.MicroblogStatusTooLongException;
import org.torproject.metrics.bot.microblog.MicroblogUpdateFailedException;
import org.torproject.metrics.bot.microblog.StatusType;
import org.torproject.metrics.bot.tor.Network;
import org.torproject.metrics.bot.tor.OnionooMissingInformationException;
import org.torproject.metrics.bot.twitter.TwitterAccount;
import org.torproject.metrics.bot.twitter.TwitterAccountConfiguration;

import org.kitteh.irc.client.library.Client;
import org.kitteh.irc.client.library.feature.auth.NickServ;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * The MetricsBot application.
 *
 * @author Iain R. Learmonth &lt;irl@torproject.org&gt;
 */
public class MetricsBot {

  static Network t;
  static Client irc;
  static List<MicroblogAccount> microblogAccounts;

  /**
   * The main method for the metrics-bot application runs through the
   * following steps:
   *
   * <ol><li>Initialise network data source (Onionoo on the backend)</li>
   * <li>Initialise the microblogging account objects</li>
   * <li>Initialise IRC client, authenticate and join channels</li>
   * <li>Start periodic timers</li></ol>
   *
   * <p>The main function then returns with all later functions running
   * in timer threads.
   */
  public static void main(String[] args) {
    // Initialise network data source
    t = new Network();
    t.update();

    // Read configuration from ~/.metrics-bot.xml
    Configuration configuration = Configuration.getSingleton();

    // Initialise microblogging accounts
    microblogAccounts = new ArrayList<>();
    for (TwitterAccountConfiguration a : configuration.getTwitterAccounts()) {
      microblogAccounts.add(new TwitterAccount(a));
    }
    for (MastodonAccountConfiguration a : configuration.getMastodonAccounts()) {
      microblogAccounts.add(new MastodonAccount(a));
    }

    // Initialise IRC
    irc = Client.builder().nick(configuration.getIrcNick())
            .serverHost("irc.oftc.net").realName(configuration.getIrcNick())
            .build();
    irc.getAuthManager().addProtocol(
            new NickServ(irc, configuration.getIrcNickServPassword(),
                    configuration.getIrcNick()));
    FactoidManager.getSingleton()
            .addFactoids(FactoidConfiguration.getSingleton().getFactoids());
    irc.getEventManager().registerEventListener(new IrcListener());
    for (IrcChannelConfiguration a : configuration.getIrcChannels()) {
      irc.addChannel(a.getChannelName());
      microblogAccounts.add(new IrcChannel(a));
    }

    // Start periodic timers
    startRelayTweetTimer(1000 * 60 * 46); // every 46 minutes
    startNetworkTweetTimer(1000 * 60 * 60 * 4); // every 4 hours
    startUpdateTimer();

    error("started up");
  }

  /**
   * Makes a random tweet periodically.
   *
   * @param milliseconds
   *          How often in milliseconds tweets should be posted.
   */
  private static void startNetworkTweetTimer(int milliseconds) {
    Timer tim = new Timer();
    tim.schedule(new TimerTask() {
      @Override
      public void run() {
        try {
          microblog(t.getNetworkTweet());
        } catch (OnionooMissingInformationException e) {
          e.printStackTrace();
          // do nothing, this isn't going anywhere and
          // one missed tweet won't hurt
        }
      }
    }, 0, milliseconds);
  }

  /**
   * Makes a random tweet periodically.
   *
   * @param milliseconds
   *          How often in milliseconds tweets should be posted.
   */
  private static void startRelayTweetTimer(int milliseconds) {
    Timer tim = new Timer();
    tim.schedule(new TimerTask() {
      @Override
      public void run() {
        try {
          microblog(t.getRelayTweet());
        } catch (OnionooMissingInformationException e) {
          e.printStackTrace();
          // do nothing, this isn't going anywhere and
          // one missed tweet won't hurt
        }
      }
    }, 0, milliseconds);
  }

  /**
   * Performs an update of the cached data from Onionoo.
   */
  private static void startUpdateTimer() {
    Timer tim = new Timer();
    tim.schedule(new TimerTask() {
      @Override
      public void run() {
        t.update();
        System.out.println("Updated Tor Network information from Onionoo");
      }
    }, 3600000, 3600000);
  }

  public static Client getIrcClient() {
    return irc;
  }

  public static Network getTorNetwork() {
    return t;
  }

  /**
   * Microblog a status message to all configured accounts accepting the
   * status' type.
   */
  public static void microblog(MicroblogStatus status) {
    for (MicroblogAccount a : microblogAccounts) {
      try {
        a.updateStatus(status);
      } catch (MicroblogStatusTooLongException e) {
        error("Message too long for " + a.getFriendlyName() + ": [" + status
                + "] (" + status.getText().length() + "," + e.getMaxLongness()
                + ")");
      } catch (MicroblogUpdateFailedException e) {
        // Log some output, but let it go
        e.printStackTrace();
      }
    }
  }

  /**
   * A wrapper for {@link #microblog(MicroblogStatus)} that also prints
   * to the console.
   */
  public static void error(String text) {
    System.err.println(text);
    microblog(new MicroblogStatus("[metrics-bot] " + text + ", cc: irl", null,
            StatusType.ALERT));
  }
}
