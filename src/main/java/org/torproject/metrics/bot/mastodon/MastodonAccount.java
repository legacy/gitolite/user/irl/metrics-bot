package org.torproject.metrics.bot.mastodon;

import org.torproject.metrics.bot.MetricsBot;
import org.torproject.metrics.bot.microblog.BaseMicroblogAccountImpl;
import org.torproject.metrics.bot.microblog.MicroblogAccount;
import org.torproject.metrics.bot.microblog.MicroblogStatusTooLongException;
import org.torproject.metrics.bot.microblog.MicroblogUpdateFailedException;

import com.google.gson.Gson;
import com.sys1yagi.mastodon4j.MastodonClient;
import com.sys1yagi.mastodon4j.MastodonRequest;
import com.sys1yagi.mastodon4j.api.entity.Attachment;
import com.sys1yagi.mastodon4j.api.entity.Status;
import com.sys1yagi.mastodon4j.api.exception.Mastodon4jRequestException;
import com.sys1yagi.mastodon4j.api.method.Media;
import com.sys1yagi.mastodon4j.api.method.Statuses;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

import java.awt.image.RenderedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

/**
 * Microblogging interface to a Mastodon account.
 *
 * @author Iain R. Learmonth &lt;irl@torproject.org&gt;
 */
public class MastodonAccount extends BaseMicroblogAccountImpl
        implements MicroblogAccount {

  private String friendlyName;
  private MastodonClient client;

  /**
   * Creates a new Mastodon account instance using instance and OAuth
   * credentials as specified in the configuration.
   *
   * @param configuration
   *          The configuration specific to the Mastodon account.
   */
  public MastodonAccount(MastodonAccountConfiguration configuration) {
    super(configuration);
    friendlyName = configuration.getFriendlyName();
    client = new MastodonClient.Builder(configuration.getInstanceHost(),
            new OkHttpClient.Builder(), new Gson())
                    .accessToken(configuration.getAccessToken()).build();
  }

  private List<Long> uploadMedia(RenderedImage image)
          throws MicroblogUpdateFailedException {
    List<Long> mediaIds;
    Media media = new Media(client);
    ByteArrayOutputStream os = new ByteArrayOutputStream();
    try {
      ImageIO.write(image, "png", os);
    } catch (IOException e) {
      e.printStackTrace();
      return null;
    }
    RequestBody body = RequestBody.create(MediaType.parse("image/png"),
            os.toByteArray());
    MultipartBody.Part part = MultipartBody.Part.createFormData("file",
            "metrics-bot.png", body);
    MastodonRequest<Attachment> mediaRequest = media.postMedia(part);
    int attempt = 0;
    while (true) {
      try {
        Attachment attachment = mediaRequest.execute();
        mediaIds = new ArrayList<Long>();
        mediaIds.add(attachment.getId());
        return mediaIds;
      } catch (Mastodon4jRequestException e) {
        if (attempt++ == 3) {
          MetricsBot.error(
                  "Failed to handle image for a status update for "
                          + "Mastodon account: " + friendlyName);
          throw new MicroblogUpdateFailedException();
        }
      }
    }
  }

  @Override
  public void updateStatus(String text, RenderedImage image)
          throws MicroblogUpdateFailedException {
    if (text.length() > 500) {
      throw new MicroblogStatusTooLongException(500);
    }

    List<Long> mediaIds = null;
    if (image != null) {
      mediaIds = uploadMedia(image);
    }

    Statuses statuses = new Statuses(client);
    int attempt = 0;
    while (true) {
      try {
        MastodonRequest<Status> statusRequest = statuses.postStatus(text, null,
                mediaIds, false, null);
        Status status = statusRequest.execute();
        System.out.println("Successfully updated the status for " + friendlyName
                + " to [" + status.getContent() + "]");
        break;
      } catch (Mastodon4jRequestException e) {
        if (attempt++ == 3) {
          MetricsBot.error(
                  "Failed to perform a status update for Mastodon account "
                          + "after 3 attempts: " + friendlyName);
          throw new MicroblogUpdateFailedException();
        }
      }
    }
  }

  @Override
  public String getFriendlyName() {
    return friendlyName;
  }

}
