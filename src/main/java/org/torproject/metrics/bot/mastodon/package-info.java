/**
 * Mastodon Component for metrics-bot.
 * 
 * <p>
 * The Mastodon component is a microblogging-only component in metrics-bot.
 * {@link org.torproject.metrics.bot.mastodon.MastodonAccount} implements the
 * {@link org.torproject.metrics.bot.microblog.MicroblogAccount} interface and
 * so can be used for status updates as with other microblogging components.
 * </p>
 * 
 * <p>
 * The Mastodon component supports infinite instances and infinite accounts per
 * instance. Each account can have microblogging status types (see
 * {@link org.torproject.metrics.bot.microblog.StatusType}) set to limit the
 * types of status update sent on a per-account basis.
 * </p>
 */
package org.torproject.metrics.bot.mastodon;
