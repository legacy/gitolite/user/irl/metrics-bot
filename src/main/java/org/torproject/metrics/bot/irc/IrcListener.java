package org.torproject.metrics.bot.irc;

import org.torproject.metrics.bot.MetricsBot;
import org.torproject.metrics.bot.factoids.FactoidManager;
import org.torproject.metrics.bot.tor.BaseRelay;

import net.engio.mbassy.listener.Handler;
import org.kitteh.irc.client.library.event.channel.ChannelMessageEvent;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Handlers for IRC interactive functionality.
 *
 * @author Iain R. Learmonth &lt;irl@torproject.org&gt;
 */
public class IrcListener {

  /**
   * Handles events generated when a message is received in a channel.
   * If the message was a request for a factoid then the factoid response
   * will be sent to the channel. If this message contained a relay
   * fingerprint then it send information about the relay to the
   * channel.
   */
  @Handler
  public void onChannelMessage(ChannelMessageEvent event) {
    FactoidManager factoidManager = FactoidManager.getSingleton();

    if (event.getMessage().startsWith("?selftest")) {
      event.sendReply("Hello. I am metrics-bot. Bleep. Bloop.");
      return;
    }

    if (event.getMessage().startsWith("?factoids")) {
      event.sendReply("I have " + factoidManager.size() + " factoids loaded.");
    }

    if (event.getMessage().startsWith("?")) {
      String[] parts = event.getMessage().substring(1).split(" ", 2);
      String trigger = parts[0];
      String lookup;
      if (parts.length == 2) {
        lookup = parts[1];
      } else {
        lookup = null;
      }
      String factoidResponse = factoidManager.getFactoid(trigger, lookup);
      if (factoidResponse != null) {
        event.sendReply(factoidResponse);
        return;
      }
    }

    Pattern fingerprintPattern = Pattern.compile(".*([0-9A-F]{40}).*");
    Matcher fingerprintMatcher =
            fingerprintPattern.matcher(event.getMessage().toUpperCase());
    if (fingerprintMatcher.matches()) {
      for (BaseRelay relay : MetricsBot.getTorNetwork().getRunningRelays()) {
        if (relay.getFingerprint().equals(fingerprintMatcher.group(1).trim())) {
          Boolean includeRelaySearchUrl =
                  !(Pattern.matches(".*metrics\\.torproject\\.org/rs\\.html.*",
                          event.getMessage().toLowerCase()));
          event.sendReply(relay.humanString(includeRelaySearchUrl));
          return;
        }
      }
    }
  }
}
