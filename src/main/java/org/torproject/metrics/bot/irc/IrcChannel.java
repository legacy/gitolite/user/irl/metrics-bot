package org.torproject.metrics.bot.irc;

import org.torproject.metrics.bot.MetricsBot;
import org.torproject.metrics.bot.microblog.BaseMicroblogAccountImpl;
import org.torproject.metrics.bot.microblog.MicroblogAccount;

import java.awt.image.RenderedImage;

/**
 * Microblogging interface to an IRC channel. This microblogging component does
 * not support images, so images are always discarded when status updates are
 * made.
 *
 * @author Iain R. Learmonth &lt;irl@torproject.org&gt;
 */
public class IrcChannel extends BaseMicroblogAccountImpl
        implements MicroblogAccount {

  private String friendlyName;
  private String channelName;

  @SuppressWarnings("checkstyle:javadocmethod")
  public IrcChannel(IrcChannelConfiguration configuration) {
    super(configuration);
    this.friendlyName = configuration.getFriendlyName();
    this.channelName = configuration.getChannelName();
  }

  @Override
  public void updateStatus(String text, RenderedImage image) {
    MetricsBot.getIrcClient().sendNotice(channelName, text);
    System.out.println("Successfully updated the status for " + friendlyName
            + " to [" + text + "]");
  }

  @Override
  public String toString() {
    return "<IRCChannel name=" + friendlyName + ", channelName=" + channelName
            + ">";
  }

  @Override
  public String getFriendlyName() {
    return friendlyName;
  }

}
