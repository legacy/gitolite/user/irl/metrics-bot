/**
 * IRC Component for metrics-bot.
 * 
 * <p>
 * The IRC component is an interactive component in metrics-bot.
 * {@link org.torproject.metrics.bot.irc.IrcChannel} implements the
 * {@link org.torproject.metrics.bot.microblog.MicroblogAccount} interface and
 * so can be used for status updates as with other microblogging components. The
 * {@link org.torproject.metrics.bot.irc.IrcListener} class implements handler
 * functions that are registered with the Kitteh IRC library in order to provide
 * the interactive functionality.
 * </p>
 * 
 * <p>
 * The IRC component only supports connecting to a single network, currently
 * hardcoded as OFTC, but the channels to connect to are configured via the XML
 * configuration (see {@link org.torproject.metrics.bot.Configuration}).
 * Channels can have microblogging status types (see
 * {@link org.torproject.metrics.bot.microblog.StatusType}) set but the
 * interactive functionality is not configurable on a per-channel basis.
 * </p>
 */
package org.torproject.metrics.bot.irc;
