package org.torproject.metrics.bot.irc;

import org.torproject.metrics.bot.microblog.MicroblogAccountConfiguration;
import org.torproject.metrics.bot.microblog.StatusType;

import java.util.List;

/**
 * Representation of the XML configuration for a single IRC channel.
 *
 * @author Iain R. Learmonth &lt;irl@torproject.org&gt;
 */
public class IrcChannelConfiguration implements MicroblogAccountConfiguration {

  private String name;
  private String channelName;
  private List<StatusType> statusTypes;

  @SuppressWarnings("checkstyle:javadocmethod")
  public IrcChannelConfiguration(String name, List<StatusType> statusTypes,
          String channelName) {
    this.name = name;
    this.channelName = channelName;
    this.statusTypes = statusTypes;
  }

  @Override
  public String getFriendlyName() {
    return name;
  }

  /**
   * Gets the channel name. This string is expected to start with a leading "#"
   * or other channel prefix.
   *
   * @return The channel name.
   */
  public String getChannelName() {
    return channelName;
  }

  @Override
  public List<StatusType> getStatusTypes() {
    return statusTypes;
  }

  @Override
  public String toString() {
    return "<IRCChannelConfiguration name=" + name + ", channelName="
            + channelName + ", statusTypes=" + statusTypes + ">";
  }

}
