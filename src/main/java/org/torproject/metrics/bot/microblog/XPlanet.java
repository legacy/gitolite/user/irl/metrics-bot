package org.torproject.metrics.bot.microblog;

import org.torproject.metrics.bot.tor.Relay;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

/**
 * An interface for producing maps using xplanet. This requires that the
 * <code>xplanet</code> binary be in your path.
 * 
 * @author Iain R. Learmonth &lt;irl@torproject.org&gt;
 */
class XPlanet {

  public static List<String> generateMarkers(List<Relay> relays) {
    List<String> markers = new ArrayList<String>();
    for (Relay relay : relays) {
      String color = "yellow";
      if (relay.getFlagStrings().contains("Exit")) {
        color = "green";
      } else if (relay.getFlagStrings().contains("Guard")) {
        color = "lightblue";
      } else if (relay.getFlagStrings().contains("Authority")) {
        color = "orange";
      }
      markers.add(String.format("%s %s \"\" color=%s\n", relay.getLatitude(),
              relay.getLongitude(), color));
    }
    return markers;
  }

  public static RenderedImage generateMapFromMarkers(List<String> markers)
          throws IOException, InterruptedException {
    // Create a temporary directory
    Path xplanetDir = Files.createTempDirectory("xplanet-");

    // Write configuration file
    Path confFile = Files.createFile(xplanetDir.resolve("relays-cfg"));
    BufferedWriter confWriter = Files.newBufferedWriter(confFile);
    confWriter.write(
            "[earth]\nmarker_file=markers.txt" + "\nbump_map=earthbump1k.jpg"
                    + "\nmap=earthmap1k.jpg"
                    + "\nspecular_map=earthspec1k.jpg\n");
    confWriter.close();

    // Write markers to a file
    Path markersDir = Files.createDirectory(xplanetDir.resolve("markers"));
    Path markersFile = Files.createFile(markersDir.resolve("markers.txt"));
    BufferedWriter markersWriter = Files.newBufferedWriter(markersFile);
    for (String line : markers) {
      markersWriter.write(line);
    }
    markersWriter.close();

    // Run xplanet to generate the map
    Path mapFile = Files.createTempFile("xplanet-map", ".png");
    String cmd = String.format(
            "xplanet -searchdir %s -num_times 1 -output %s -projection lambert "
                    + "-config relays-cfg -origin sun",
            xplanetDir.toString(), mapFile.toFile());
    Process xplanet = Runtime.getRuntime().exec(cmd);
    xplanet.waitFor();

    // Add overlay
    BufferedImage mapImage = ImageIO.read(mapFile.toFile());
    ClassLoader cl = XPlanet.class.getClassLoader();
    BufferedImage overlayImage = ImageIO.read(cl.getResource("overlay.png"));
    Graphics2D mapGraphics = mapImage.createGraphics();
    mapGraphics.drawImage(overlayImage, 0, 0, null);

    // Tidy up
    Files.delete(markersFile);
    Files.delete(markersDir);
    Files.delete(confFile);
    Files.delete(xplanetDir);
    Files.delete(mapFile);

    return mapImage;
  }

  public static RenderedImage generateMap(List<Relay> relays)
          throws IOException, InterruptedException {
    return generateMapFromMarkers(generateMarkers(relays));
  }
}
