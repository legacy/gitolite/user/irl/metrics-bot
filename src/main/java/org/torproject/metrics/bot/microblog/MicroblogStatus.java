package org.torproject.metrics.bot.microblog;

import java.awt.image.RenderedImage;

/**
 * A generated Microblogging status update. Any generator for microblogging
 * status updates should return an instance of this class to be used with the
 * {@link MicroblogAccount#updateStatus(MicroblogStatus)} method of individual
 * accounts. The status update should still make sense when the image is not
 * included as metrics-bot will aggressively attempt to make the update,
 * including excluding the image when an error occurs in reading the image. Some
 * implementations of {@link MicroblogAccount} may also not support images at
 * all, for example {@link org.torproject.metrics.bot.irc.IrcChannel}.
 *
 * @author Iain R. Learmonth &lt;irl@torproject.org&gt;
 */
public class MicroblogStatus {

  private String text;
  private RenderedImage image;
  private StatusType statusType;

  @SuppressWarnings("checkstyle:javadocmethod")
  public MicroblogStatus(String text, RenderedImage image,
          StatusType statusType) {
    this.text = text;
    this.image = image;
    this.statusType = statusType;
  }

  public String getText() {
    return text;
  }

  public RenderedImage getImage() {
    return image;
  }

  public StatusType getStatusType() {
    return statusType;
  }

  @Override
  public String toString() {
    return "<MicroblogStatus text=" + text + ", image=" + image + ">";
  }

}
