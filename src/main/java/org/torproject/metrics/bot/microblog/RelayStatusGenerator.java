package org.torproject.metrics.bot.microblog;

import org.torproject.metrics.bot.tor.OnionooMissingInformationException;
import org.torproject.metrics.bot.tor.Relay;

/**
 * Tweet generator functions for relay specific tweets.
 * 
 * @author Iain R. Learmonth &lt;irl@torproject.org&gt;
 */
public class RelayStatusGenerator {

  /**
   * Returns a tweet about how long the relay has been contributing to the Tor
   * network.
   * 
   * <p>An example tweet looks like:
   * 
   * <blockquote>Aramis66 in United States has been contributing to the #Tor
   * network for 154 days and 2 hours.
   * https://metrics.torproject.org/rs.html#details/637BD268843638713D6FC13A92B3E36F4B5B0F4B
   * </blockquote>
   * 
   * <p>This function requires that "country_name" has a useful value in the
   * RelayDetails object.
   * 
   * @param relay
   *          A RelayDetails object for the relay to have a tweet generated.
   * @return A tweet about how long the relay has been contributing to the Tor
   *         network.
   * @throws OnionooMissingInformationException
   *           when information required to generate the tweet is missing from
   *           the RelayDetails object (probably because Onionoo did not have
   *           it)
   */
  public static MicroblogStatus generateAgeTweet(Relay relay)
          throws OnionooMissingInformationException {
    final String template =
            "%s in %s has been contributing to the #Tor network for "
                    + "%s. https://metrics.torproject.org/rs.html#details/%s";

    String countryName = relay.getCountryName();

    if (countryName == null) {
      throw new OnionooMissingInformationException();
    }

    String humanReadableAge = MicroblogUtils.formatAge(relay.getFirstSeen());

    String tweetText =
            String.format(template, relay.getNickname(), countryName,
                    humanReadableAge, relay.getFingerprint());
    return new MicroblogStatus(tweetText, relay.generateBadge(),
            StatusType.RELAY);
  }

  /**
   * Returns a tweet about how much bandwidth a relay is contributing to the Tor
   * network.
   * 
   * <p>An example tweet looks like:
   * 
   * <blockquote>BeastieJoy60 in Germany is contributing 11.1MiB/s bandwidth to
   * the #Tor network.
   * https://metrics.torproject.org/rs.html#details/B86137AE9681701901C6720E55C16805B46BD8E3
   * </blockquote>
   * 
   * <p>This function requires that both "country_name" and
   * "advertised_bandwidth" have useful values in the RelayDetails object.
   * "advertised_bandwidth" is not considered useful is "measured" is false in
   * the Onionoo document.
   * 
   * @param relay
   *          A RelayDetails object for the relay to have a tweet generated.
   * @return A tweet about how much bandwidth the relay is contributing to the
   *         Tor network.
   * @throws OnionooMissingInformationException
   *           when information required to generate the tweet is missing from
   *           the RelayDetails object (probably because Onionoo did not have
   *           it)
   */
  public static MicroblogStatus generateBandwidthTweet(Relay relay)
          throws OnionooMissingInformationException {
    final String template =
            "%s in %s is contributing %s bandwidth to the #Tor network. "
                    + "https://metrics.torproject.org/rs.html#details/%s";

    String countryName = relay.getCountryName();
    Long advertisedBandwidth = relay.getAdvertisedBandwidth();

    if (countryName == null
            || relay.getMeasured() == false || advertisedBandwidth == 0) {
      throw new OnionooMissingInformationException();
    }

    String humanReadableAdvertisedBandwidth =
            MicroblogUtils.formatBandwidth(advertisedBandwidth);

    String tweetText =
            String.format(template, relay.getNickname(), countryName,
                    humanReadableAdvertisedBandwidth, relay.getFingerprint());
    return new MicroblogStatus(tweetText, relay.generateBadge(),
            StatusType.RELAY);
  }
}
