package org.torproject.metrics.bot.microblog;

import org.torproject.metrics.bot.tor.Country;
import org.torproject.metrics.bot.tor.CountryImpl;
import org.torproject.metrics.bot.tor.OnionooMissingInformationException;
import org.torproject.metrics.bot.tor.Relay;

import java.util.List;

/**
 * Tweet generator functions for location (Country or AS) specific tweets.
 * 
 * @author Iain R. Learmonth &lt;irl@torproject.org&gt;
 */
public class RelayLocationStatusGenerator {

  /**
   * Returns a tweet about how much bandwidth relays in a certain country are
   * contributing to the Tor network.
   * 
   * <p> An example tweet looks like: </p>
   * 
   * <blockquote>16 relays in Vietnam are contributing 7.0MiB/s bandwidth to the
   * #Tor network.
   * https://metrics.torproject.org/rs.html#search/country:vn</blockquote>
   * 
   * <p> This function requires that all the relays passed in the relays list
   * are already filtered to be from the same country. The country code and
   * country name are both determined from the first relay in the list. </p>
   * 
   * @param relays
   *          A list of RelayDetails objects for the country to have a tweet
   *          generated.
   * @return A tweet about how how much bandwidth relays in a certain country
   *         are contributing to the Tor network.
   * @throws OnionooMissingInformationException
   *           when information required to generate the tweet is missing from
   *           the RelayDetails object (probably because Onionoo did not
   *           have it)
   */
  public static MicroblogStatus generateCountryBandwidthTweet(
          List<Relay> relays) throws OnionooMissingInformationException {
    Country country = new CountryImpl(relays);
    if (country.getCountry() == null || country.getCountryName() == null) {
      throw new OnionooMissingInformationException();
    }
    String tweetText =
            generateLocationTweetText(country.getCountryName(), relays,
                    "country:" + country.getCountry());
    return new MicroblogStatus(tweetText, country.generateBadge(),
            StatusType.RELAY);
  }

  /**
   * Returns a tweet about how much bandwidth relays in a certain AS are
   * contributing to the Tor network.
   * 
   * <p> An example tweet looks like: </p>
   * 
   * <blockquote>5 relays in EE Limited (AS12576) are contributing 2.4MiB/s
   * bandwidth to the #Tor network.
   * https://metrics.torproject.org/rs.html#search/as:12576 </blockquote>
   * 
   * <p> This function requires that all the relays passed in the relays list
   * are already filtered to be from the same AS. The AS number and AS name are
   * both determined from the first relay in the list. </p>
   * 
   * @param relays
   *          A list of RelayDetails objects for the AS to have a tweet
   *          generated.
   * @return A tweet about how how much bandwidth relays in a certain AS are
   *         contributing to the Tor network.
   * @throws OnionooMissingInformationException
   *           when information required to generate the tweet is missing from
   *           the RelayDetails object (probably because Onionoo did not
   *           have it)
   */
  public static MicroblogStatus generateAsBandwidthTweet(List<Relay> relays)
          throws OnionooMissingInformationException {
    String asNumber = relays.get(0).getAsNumber();
    String asName = relays.get(0).getAsName();
    if (asNumber == null || asName == null) {
      throw new OnionooMissingInformationException();
    }
    String tweetText =
            generateLocationTweetText(asName + " (" + asNumber + ")", relays,
                    "as:" + asNumber.substring(2));
    return new MicroblogStatus(tweetText, null, StatusType.RELAY);
  }

  /**
   * Helper to produce bandwidth contribution tweets for locations. This is
   * intended to be called from a wrapper class and only performs counting of
   * relays, summing of bandwidth and formatting the final tweet using the
   * provided information.
   * 
   * @param location
   *          The human readable location for the tweet.
   * @param relays
   *          A list of relays that match the location for the tweet.
   * @param searchQuery
   *          The Relay Search query to search for relays matching the location.
   * @return A formatted tweet.
   */
  public static String generateLocationTweetText(String location,
          List<Relay> relays, String searchQuery) {
    final String template =
            "%d relays in %s are contributing %s bandwidth to "
                    + "the #Tor network. "
                    + "https://metrics.torproject.org/rs.html#search/%s";
    Integer relayCount = relays.size();
    Long advertisedBandwidth =
            relays.stream().mapToLong(i -> i.getAdvertisedBandwidth()).sum();
    String humanReadableAdvertisedBandwidth =
            MicroblogUtils.formatBandwidth(advertisedBandwidth);
    return String.format(template, relayCount, location,
            humanReadableAdvertisedBandwidth, searchQuery);
  }
}
