package org.torproject.metrics.bot.microblog;

import org.torproject.metrics.bot.MetricsBot;
import org.torproject.metrics.bot.tor.Relay;

import java.awt.image.RenderedImage;
import java.io.IOException;
import java.util.List;

public class NetworkStatusGenerator {

  /**
   * Generates a tweet about the total bandwidth being made available by Tor
   * relays. This also uses XPlanet to create a world map of all the relays
   * and attaches this to the tweet.
   */
  public static MicroblogStatus generateMapTweet(List<Relay> relays) {
    String template = "There are currently %d #Tor relays running providing %s total bandwidth. https://metrics.torproject.org/networksize.html";
    Long advertisedBandwidth = relays.stream()
            .mapToLong(i -> i.getAdvertisedBandwidth()).sum();
    String humanReadableAdvertisedBandwidth = MicroblogUtils
            .formatBandwidth(advertisedBandwidth);
    String tweetText = String.format(template, relays.size(),
            humanReadableAdvertisedBandwidth);
    RenderedImage tweetImage = null;
    try {
      tweetImage = XPlanet.generateMap(relays);
    } catch (IOException | InterruptedException e) {
      MetricsBot.error("Failed to run xplanet!");
      e.printStackTrace();
    }
    return new MicroblogStatus(tweetText, tweetImage, StatusType.NETWORK);
  }

}
