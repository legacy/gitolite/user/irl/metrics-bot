package org.torproject.metrics.bot.microblog;

import java.awt.image.RenderedImage;
import java.util.List;

public abstract class BaseMicroblogAccountImpl {

  List<StatusType> statusTypes;

  public BaseMicroblogAccountImpl(MicroblogAccountConfiguration configuration) {
    statusTypes = configuration.getStatusTypes();
  }

  /**
   * A helper function to allow updating the status using a
   * {@link MicroblogStatus} object. Internally uses
   * {@link #updateStatus(String, RenderedImage)}.
   */
  public void updateStatus(MicroblogStatus status)
          throws MicroblogUpdateFailedException {
    if (statusTypes.contains(status.getStatusType())) {
      updateStatus(status.getText(), status.getImage());
    }
  }

  public abstract void updateStatus(String text, RenderedImage image)
          throws MicroblogUpdateFailedException;

  public List<StatusType> getStatusTypes() {
    return statusTypes;
  }

}
