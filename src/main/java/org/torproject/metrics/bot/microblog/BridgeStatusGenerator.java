package org.torproject.metrics.bot.microblog;

import org.torproject.metrics.bot.tor.Bridge;
import org.torproject.metrics.bot.tor.OnionooMissingInformationException;

public class BridgeStatusGenerator {

  /**
   * Returns a tweet about how long the bridge has been contributing to the Tor
   * network.
   *
   * @param bridge
   *          A BridgeDetails object for the relay to have a tweet generated.
   * @return A tweet about how long the relay has been contributing to the Tor
   *         network.
   * @throws OnionooMissingInformationException
   *           when information required to generate the tweet is missing from
   *           the BridgeDetails object (probably because Onionoo did not have
   *           it)
   */
  public static MicroblogStatus generateAgeTweet(Bridge bridge)
          throws OnionooMissingInformationException {
    final String template =
            "%s has been contributing censorship-resistant access bandwidth to "
                    + "the #Tor network for %s with %s. "
                    + "https://metrics.torproject.org/rs.html#details/%s";

    String humanReadableAge = MicroblogUtils.formatAge(bridge.getFirstSeen());

    try {
      String tweetText =
              String.format(template, bridge.getNickname(), humanReadableAge,
                      bridge.getTransports().get(0), bridge.getFingerprint());
      return new MicroblogStatus(tweetText, bridge.generateBadge(),
              StatusType.RELAY);
    } catch (NullPointerException e) {
      throw new OnionooMissingInformationException();
    }
  }

  /**
   * Returns a tweet about how much bandwidth a bridge has been contributing to
   * the Tor network.
   *
   * @param bridge
   *          A BridgeDetails object for the relay to have a tweet generated.
   * @return A tweet about how long the relay has been contributing to the Tor
   *         network.
   * @throws OnionooMissingInformationException
   *           when information required to generate the tweet is missing from
   *           the BridgeDetails object (probably because Onionoo did not have
   *           it)
   */
  public static MicroblogStatus generateBandwidthTweet(Bridge bridge)
          throws OnionooMissingInformationException {
    final String template =
            "%s is contributing %s censorship-resistant access bandwidth to "
                    + "the #Tor network with %s. "
                    + "https://metrics.torproject.org/rs.html#details/%s";

    Long advertisedBandwidth = bridge.getAdvertisedBandwidth();

    if (advertisedBandwidth == 0) {
      throw new OnionooMissingInformationException();
    }

    String humanReadableAdvertisedBandwidth =
            MicroblogUtils.formatBandwidth(advertisedBandwidth);

    try {
      String tweetText =
              String.format(template, bridge.getNickname(),
                      humanReadableAdvertisedBandwidth,
                      bridge.getTransports().get(0), bridge.getFingerprint());
      return new MicroblogStatus(tweetText, bridge.generateBadge(),
              StatusType.RELAY);
    } catch (NullPointerException e) {
      throw new OnionooMissingInformationException();
    }
  }
}
