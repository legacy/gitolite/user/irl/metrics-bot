package org.torproject.metrics.bot.microblog;

import java.util.List;

public interface MicroblogAccountConfiguration {

  /**
   * Gets the friendly name for this account.
   * 
   * @return The friendly name.
   */
  String getFriendlyName();

  /**
   * Gets the list of desirable status update types for this account.
   * 
   * @return The list of desirable status update types.
   * @see StatusType
   */
  List<StatusType> getStatusTypes();

}
