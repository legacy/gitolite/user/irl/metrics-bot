package org.torproject.metrics.bot.factoids;

import java.util.List;

public class StaticFactoid implements Factoid {

  private List<String> triggers;
  private String response;

  public StaticFactoid(List<String> triggers, String response) {
    this.triggers = triggers;
    this.response = response;
  }

  @Override
  public Boolean isTriggered(String trigger) {
    return triggers.contains(trigger);
  }

  @Override
  public String getFactoid(String lookup) {
    return response;
  }

}
