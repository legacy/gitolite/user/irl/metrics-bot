package org.torproject.metrics.bot.factoids;

import org.torproject.metrics.bot.MetricsBot;
import org.torproject.metrics.bot.microblog.NetworkStatusGenerator;

public class NetworkSizeFactoid implements Factoid {

  @Override
  public Boolean isTriggered(String trigger) {
    return trigger.equals("relays");
  }

  @Override
  public String getFactoid(String lookup) {
    return NetworkStatusGenerator
            .generateMapTweet(MetricsBot.getTorNetwork().getRunningRelays())
            .getText();
  }

}
