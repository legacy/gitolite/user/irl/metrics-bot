package org.torproject.metrics.bot.factoids;

import org.torproject.metrics.bot.MetricsBot;
import org.torproject.metrics.bot.microblog.RelayLocationStatusGenerator;
import org.torproject.metrics.bot.tor.OnionooMissingInformationException;
import org.torproject.metrics.bot.tor.Relay;

import java.util.List;

public class AutonomousSystemFactoid implements Factoid {

  @Override
  public Boolean isTriggered(String trigger) {
    return trigger.equals("as");
  }

  @Override
  public String getFactoid(String lookup) {
    Integer asNumber;
    try {
      asNumber = Integer.parseInt(lookup);
    } catch (NumberFormatException e) {
      return "I can only identify autonomous systems by their number."
              + " Do not add a leading \"AS\". For example: ?as 3";
    }
    List<Relay> relays =
            MetricsBot.getTorNetwork()
                    .getRunningRelaysForAs("AS" + asNumber.toString());
    try {
      return RelayLocationStatusGenerator.generateAsBandwidthTweet(relays)
              .getText();
    } catch (IndexOutOfBoundsException e) {
      return "I don't know that AS. Sorry. Bleep bloop.";
    } catch (OnionooMissingInformationException e) {
      return "Something went wrong. Sorry.";
    }
  }

}
