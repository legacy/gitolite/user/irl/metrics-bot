package org.torproject.metrics.bot.factoids;

import java.util.ArrayList;
import java.util.List;

public class FactoidManager {

  private static FactoidManager factoidManager;

  private List<Factoid> factoids;

  public FactoidManager() {
    factoids = new ArrayList<Factoid>();
  }

  public void addFactoid(Factoid factoid) {
    factoids.add(factoid);
  }

  public void addFactoids(List<Factoid> factoids) {
    this.factoids.addAll(factoids);
  }

  /**
   * Searches the configured factoids for the first that triggers
   * and passes it the lookup parameter. If no factoid triggered,
   * this will return null.
   */
  public String getFactoid(String trigger, String lookup) {
    for (Factoid f : factoids) {
      if (f.isTriggered(trigger)) {
        return f.getFactoid(lookup);
      }
    }
    return null;
  }

  public int size() {
    return factoids.size();
  }

  @SuppressWarnings("checkstyle:javadocmethod")
  public static FactoidManager getSingleton() {
    if (factoidManager == null) {
      factoidManager = new FactoidManager();
    }
    return factoidManager;
  }
}
