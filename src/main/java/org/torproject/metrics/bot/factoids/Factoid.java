package org.torproject.metrics.bot.factoids;

/**
 * Generates a response to an interactive query when a trigger word is used.
 *
 * @author Iain R. Learmonth &lt;irl@torproject.org&gt;
 */
public interface Factoid {

  /**
   * Checks if a word a trigger word for this factoid.
   *
   * @param trigger
   *          the word to check
   * @return true if the word is a trigger word for this factoid, otherwise
   *         false
   */
  Boolean isTriggered(String trigger);

  /**
   * Returns the factoid's response based on a lookup parameter. The lookup
   * parameter may be null, but in this case the response may just be a human
   * readable error message.
   *
   * @param lookup
   *          a parameter to pass to the factoid that may influence the response
   * @return a human readable response to the lookup parameter
   */
  String getFactoid(String lookup);

}
